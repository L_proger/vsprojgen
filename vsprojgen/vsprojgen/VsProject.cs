﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;

namespace vsprojgen {
    public enum OutputType {
        Library
    }

    public enum FrameworkVersion {
        v3_5,
        v4_5_1,
        v4_5_3
    }
    public enum ToolsVersion {
        v14_0
    }

    public enum Platform {
        AnyCPU,
        x64,
        x86
    }

    public enum DebugType {
        full,
        pdbonly
    }

    public enum ErrorReport {
        prompt
    }

    public class ConfigurationProperties {
        public Platform Platform;
        public Platform PlatformTarget = Platform.AnyCPU;
        public Configuration Configuration;
        public bool DebugSymbols;
        public DebugType DebugType;
        public bool Optimize;
        public string OutputPath;
        public List<string> DefineConstants = new List<string>();
        public ErrorReport ErrorReport;
        public int WarningLevel;


        public string CombinedConstants {
            get {
                return DefineConstants.Aggregate((l, r) => l + ";" + r);
            }
        }
    }

    public static class DefaultConstants {
        public const string DEBUG = "DEBUG";
        public const string TRACE = "TRACE";
    }

    public enum Configuration {
        Debug,
        Release
    }

    public class ProjectReference {
        public string Name;
        public string HintPath;

        public ProjectReference(string name, string hintPath = "") {
            Name = name;
            HintPath = hintPath;
        }
    }

    public enum ProjectFileType {
        Compile,
        None,
        Content
    }

    public class ProjectFile {
        public string Path;
        public ProjectFileType FileType;
        public bool IsLink;

        public ProjectFile(string path, ProjectFileType fileType, bool isLink = false) {
            Path = path;
            FileType = fileType;
            IsLink = isLink;
        }
    }

    public class VsProject {
        public OutputType OutputType;
        public string RootNamespace;
        public string AssemblyName;
        public FrameworkVersion TargetFrameworkVersion;
        public ToolsVersion ToolsVersion;
        public Guid ProjectGuid;
        public Platform Platform;
        public Configuration Configuration;
        public List<ConfigurationProperties> Configurations = new List<ConfigurationProperties>();
        public List<ProjectReference> References = new List<ProjectReference>();
        public List<ProjectFile> Files = new List<ProjectFile>();
        public string BaseDirectory;

        public void GenerateProjectFile(Stream outStream) {
            if(outStream == null || !outStream.CanWrite) {
                return;
            }
            var document = new XDocument(new XDeclaration("1.0", "utf-8", ""));
            XNamespace ns = @"http://schemas.microsoft.com/developer/msbuild/2003";
            var root = new XElement(ns + "Project");
            root.Add(new XAttribute("ToolsVersion", DemangleVersionString(ToolsVersion.ToString())));
            root.Add(new XAttribute("DefaultTargets", "Build"));
            document.Add(root);

            //serialize global config
            var globalConfig = new XElement(ns + "PropertyGroup");
            globalConfig.Add(new XElement(ns + "Platform", new XAttribute("Condition", " '$(Platform)' == '' "), this.Platform.ToString()));
            globalConfig.Add(new XElement(ns + "ProjectGuid", "{"+ ProjectGuid.ToString().ToUpper() + "}"));
            globalConfig.Add(new XElement(ns + "OutputType", OutputType.ToString()));
            globalConfig.Add(new XElement(ns + "AppDesignerFolder", "Properties"));
            globalConfig.Add(new XElement(ns + "RootNamespace", RootNamespace));
            globalConfig.Add(new XElement(ns + "AssemblyName", AssemblyName));
            globalConfig.Add(new XElement(ns + "TargetFrameworkVersion", DemangleVersionString(TargetFrameworkVersion.ToString())));
            globalConfig.Add(new XElement(ns + "FileAlignment", "512"));
            globalConfig.Add(new XElement(ns + "BaseDirectory", BaseDirectory));
            root.Add(globalConfig);

            //serialize configurations
            foreach(var config in Configurations) {
                if(config != null) {
                    root.Add(Serialize(ns, config));
                }
            }

            //serialize references
            var refs = new XElement(ns + "ItemGroup");
            foreach(var reference in References) {
                refs.Add(Serialize(ns, reference));
            }
            root.Add(refs);


            //add source files list
            var filesGroups = Files.GroupBy(v => v.FileType);
            foreach(var fileGroup in filesGroups) {
                var files = new XElement(ns + "ItemGroup");
                foreach(var file in fileGroup) {
                    files.Add(Serialize(ns, file));
                }
                root.Add(files);
            }


            //add unknown thing
            var unknwn = new XElement(ns + "Import");
            unknwn.Add(new XAttribute("Project", @"$(MSBuildToolsPath)\Microsoft.CSharp.targets"));
            root.Add(unknwn);


            root.Add(new XComment("To modify your build process, add your task inside one of the targets below and uncomment it. \n" +
                    "Other similar extension points exist, see Microsoft.Common.targets.\n" +
                    "< Target Name=\"BeforeBuild\" > \n" +
                    "</ Target >\n" +
                    "< Target Name = \"AfterBuild\" > \n" +
                    "</ Target >\n"));
            document.Save(outStream);
        }

        private string DemangleVersionString(string ver) {
            return ver.Replace("_", ".");
        }

        private XElement Serialize(XNamespace ns, ConfigurationProperties config) {
            var result = new XElement(ns + "PropertyGroup");
            result.Add(new XAttribute("Condition", string.Format(" '$(Configuration)|$(Platform)' == '{0}|{1}' ", config.Configuration, config.Platform)));

            if(config.Configuration == Configuration.Debug) {
                result.Add(new XElement(ns + "DebugSymbols", config.DebugSymbols.ToString()));
            }

            result.Add(new XElement(ns + "DebugType", config.DebugType.ToString()));
            result.Add(new XElement(ns + "Optimize", config.Optimize.ToString()));
            result.Add(new XElement(ns + "OutputPath", config.OutputPath));
            result.Add(new XElement(ns + "DefineConstants", config.CombinedConstants));
            result.Add(new XElement(ns + "ErrorReport", config.ErrorReport.ToString()));
            result.Add(new XElement(ns + "WarningLevel", config.WarningLevel.ToString()));

            if(config.PlatformTarget != Platform.AnyCPU) {
                result.Add(new XElement(ns + "PlatformTarget", config.PlatformTarget.ToString()));
            }


            return result;
        }

        private XElement Serialize(XNamespace ns, ProjectReference reference) {
            var result = new XElement(ns + "Reference");
            result.Add(new XAttribute("Include", reference.Name));
            if(!string.IsNullOrEmpty(reference.HintPath)) {
                result.Add(new XElement(ns + "HintPath", reference.HintPath));
            }
            return result;
        }

        private XElement Serialize(XNamespace ns, ProjectFile file) {
            var result = new XElement(ns + file.FileType.ToString());
            result.Add(new XAttribute("Include", file.Path));

            if(file.IsLink) {
                var link = new XElement(ns + "Link");
                link.Add(Path.GetFileName(file.Path));
                result.Add(link);
            }

            return result;
        }
    }
}
