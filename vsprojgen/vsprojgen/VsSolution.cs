﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace vsprojgen
{
    public enum SolutionFileVersion {
        v12_0
    }

    public abstract class VsSolutionBlock {
        public List<VsSolutionBlock> Children = new List<VsSolutionBlock>();

        public abstract void Serialize(Stream outStream, int treeDepth = 0);

        public void SerializeChildren(Stream outStream, int treeDepth) {
            if(Children == null) {
                return;
            }
            foreach(var child in Children) {
                child.Serialize(outStream, treeDepth);
            }
        }

        public string GetIndentString(int depth) {
            string result = "";
            for(int i = 0; i < depth; ++i) {
                result += "\t";
            }
            return result;
        }
    }

    public class VsSolutionNodeParameter {
        public string Name;
        public string Value;

        public VsSolutionNodeParameter(string name, string value) {
            Name = name;
            Value = value;
        }

        public string Serialize() {
            if(string.IsNullOrEmpty(Name)) {
                return string.Format("{0}", Value);
            } else {
                return string.Format("({0}) = {1}", Name, Value);
            }
        }
    }

    public class VsSolutionNodeBlock : VsSolutionBlock {
        public string Name;

        public VsSolutionNodeBlock(string name) {
            Name = name;
        }

        public List<VsSolutionNodeParameter> Parameters = new List<VsSolutionNodeParameter>();

        public override void Serialize(Stream outStream, int treeDepth = 0) {
            var indent = GetIndentString(treeDepth);
            var sw = new StreamWriter(outStream);
            sw.WriteLine(indent + Name + SerializeParameters());
            sw.Flush();
            SerializeChildren(outStream, treeDepth + 1);
            sw.WriteLine(indent + "End" + Name);
            sw.Flush();
        }

        private string SerializeParameters() {
            if(Parameters.Count == 0) {
                return "";
            }
            return Parameters.Select(v => v.Serialize()).Aggregate((a, b) => a + ", " + b);
        }
    }

    public class VsSolutionNodeText : VsSolutionBlock {
        public List<string> Lines = new List<string>();

        public VsSolutionNodeText() {

        }
        public VsSolutionNodeText(string line) {
            Lines.Add(line);
        }

        public VsSolutionNodeText(string[] lines) {
            Lines.AddRange(lines);
        }

        public override void Serialize(Stream outStream, int treeDepth = 0) {
            var indent = GetIndentString(treeDepth);
            var sw = new StreamWriter(outStream);
            foreach(var s in Lines) {
                sw.WriteLine(indent + s);
            }
            sw.Flush();
        }
    }

    public class VsProjectLink {
        public VsProject Project;
        public string SolutionRelativePath;

        public VsProjectLink(VsProject project, string relativePath) {
            Project = project;
            SolutionRelativePath = relativePath;
        }
    }

    public class VsSolution
    {
        public Guid Guid;
        public List<VsProjectLink> Projects = new List<VsProjectLink>();
        public SolutionFileVersion Version;
        public string Name = "";

        public static string GuidToStr(Guid g) {
            return "{" + g.ToString().ToUpper() + "}";
        }

        public void Serialize(Stream outStream) {
            WriteHeader(outStream);

            //write projects
            foreach(var project in Projects) {
                var sh = new VsSolutionNodeBlock("Project");
                sh.Parameters.Add(new VsSolutionNodeParameter("\"" + GuidToStr(Guid) + "\"", "\"" + Name + "\""));
                sh.Parameters.Add(new VsSolutionNodeParameter(null, "\"" + project.SolutionRelativePath + "\""));
                sh.Parameters.Add(new VsSolutionNodeParameter(null, "\"" + GuidToStr(project.Project.ProjectGuid) + "\""));
                sh.Serialize(outStream);
            }

            //global section
            var global = new VsSolutionNodeBlock("Global");

            //pre solution config
            var globalSection = new VsSolutionNodeBlock("GlobalSection");
            global.Children.Add(globalSection);
            globalSection.Parameters.Add(new VsSolutionNodeParameter("SolutionConfigurationPlatforms", "preSolution"));
            var preSolutionParams = new VsSolutionNodeText();
            globalSection.Children.Add(preSolutionParams);

            foreach(var project in Projects) {
                var configGroups = project.Project.Configurations.GroupBy(v => v.Configuration);
                //foreach config group
                foreach(var confGroup in configGroups) {
                    foreach(var conf in confGroup) {
                        var str = string.Format("{0}|{1} = {0}|{1}", conf.Configuration.ToString(), PlatformString(conf.Platform));
                        preSolutionParams.Lines.Add(str);
                    }
                }
            }

            //post solution config
            var projectsConfig = new VsSolutionNodeBlock("GlobalSection");
            projectsConfig.Parameters.Add(new VsSolutionNodeParameter("ProjectConfigurationPlatforms", "postSolution"));
            global.Children.Add(projectsConfig);
            var projectsConfigText = new VsSolutionNodeText();
            projectsConfig.Children.Add(projectsConfigText);

            foreach(var project in Projects) {
                var configGroups = project.Project.Configurations.GroupBy(v => v.Configuration);
                var projectGuidStr = GuidToStr(project.Project.ProjectGuid);
                //foreach config group
                foreach(var confGroup in configGroups) {
                    foreach(var conf in confGroup) {
                        projectsConfigText.Lines.Add(string.Format("{0}.{1}|{2}.ActiveCfg = {1}|{2}", projectGuidStr, conf.Configuration, PlatformString(conf.Platform)));
                        projectsConfigText.Lines.Add(string.Format("{0}.{1}|{2}.Build.0 = {1}|{2}", projectGuidStr, conf.Configuration, PlatformString(conf.Platform)));
                    }
                }
            }


            var solutionProperties = new VsSolutionNodeBlock("GlobalSection");
            solutionProperties.Parameters.Add(new VsSolutionNodeParameter("SolutionProperties", "preSolution"));
            solutionProperties.Children.Add(new VsSolutionNodeText("HideSolutionNode = FALSE"));
            global.Children.Add(solutionProperties);


            global.Serialize(outStream);
        }

        private string PlatformString(Platform p) {
            if(p == Platform.AnyCPU) {
                return "Any CPU";
            }
            return p.ToString();
        }

        private string DemangleVersionString(string ver) {
            return ver.Replace("v", "").Replace("_", ".");
        }

        private void WriteHeader(Stream outStream) {
            StreamWriter sw = new StreamWriter(outStream);
            sw.WriteLine(string.Format("Microsoft Visual Studio Solution File, Format Version {0}", DemangleVersionString(Version.ToString())));
            sw.WriteLine(string.Format("# Visual Studio {0}", "14"));
            sw.WriteLine(string.Format("VisualStudioVersion = {0}", "14.0.22310.1"));
            sw.WriteLine(string.Format("MinimumVisualStudioVersion = {0}", "10.0.40219.1"));
            sw.Flush();
        }
    }
}
