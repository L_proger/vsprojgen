﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vsprojgen {
    public static class VsUtils {
        public static string MakeRelativePath(string from, string to) {
            Uri fullPath = new Uri(to, UriKind.Absolute);
            Uri relRoot = new Uri(from, UriKind.Absolute);
            return relRoot.MakeRelativeUri(fullPath).ToString();
        }
    }
}
