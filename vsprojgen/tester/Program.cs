﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vsprojgen;

namespace tester {
    class Program {
        static void Main(string[] args) {

            string projectPath = @"C:\Users\Sergey\Desktop\Test.csproj";
            string solutionPath = @"C:\Users\Sergey\Desktop\Test.sln";

            var project = new VsProject();
            project.AssemblyName = "ImageProcessorScripts";
            project.RootNamespace = project.AssemblyName;
            project.ProjectGuid = Guid.NewGuid();
            project.TargetFrameworkVersion = FrameworkVersion.v3_5;
            project.OutputType = OutputType.Library;
            project.Platform = Platform.AnyCPU;
            project.Configuration = Configuration.Debug;
            project.BaseDirectory = "Assets";

            //add configurations
            //Debug config
            project.Configurations.Add(new ConfigurationProperties() {
                Configuration = Configuration.Debug,
                DebugSymbols = true,
                DebugType = DebugType.full,
                DefineConstants = new List<string>(new[] { DefaultConstants.DEBUG, DefaultConstants.TRACE}),
                ErrorReport = ErrorReport.prompt,
                Optimize = false,
                OutputPath = @"Temp\bin\Debug",
                Platform = Platform.AnyCPU,
                WarningLevel = 4
            });

            //Release config
            project.Configurations.Add(new ConfigurationProperties()
            {
                Configuration = Configuration.Release,
                DebugType = DebugType.pdbonly,
                DefineConstants = new List<string>(new[] { DefaultConstants.TRACE }),
                ErrorReport = ErrorReport.prompt,
                Optimize = true,
                OutputPath = @"Temp\bin\Release",
                Platform = Platform.AnyCPU,
                WarningLevel = 4
            });

            //Add project files
            project.Files.Add(new ProjectFile("Script1.cs", ProjectFileType.Compile));
            project.Files.Add(new ProjectFile("Shader1.hlsl", ProjectFileType.Content));
            project.Files.Add(new ProjectFile("Shader2.hlslinc", ProjectFileType.Content));

            //Referencing external DLL-s
            project.References.AddRange(new[] {
                new ProjectReference("System"),
                new ProjectReference("System.Core"),
                new ProjectReference("ImageProcessor", "../ImageProcessor.dll")});

            //Generate project
            var outStream = Console.OpenStandardOutput();
            var stm = System.IO.File.OpenWrite(projectPath);
           
            project.GenerateProjectFile(stm);
            stm.Close();

            stm = System.IO.File.OpenWrite(solutionPath);
            var solution = new VsSolution();
            solution.Name = "Test";
            solution.Version = SolutionFileVersion.v12_0;
            solution.Guid = Guid.NewGuid();
            solution.Projects.Add(new VsProjectLink(project, VsUtils.MakeRelativePath(solutionPath, projectPath)));
            solution.Serialize(stm);
            stm.Close();

          

            //Console.Read();
        }
    }
}
